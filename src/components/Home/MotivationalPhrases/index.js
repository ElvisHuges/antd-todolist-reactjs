import React from "react";
import { Card, Col, Row, Layout } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from "@ant-design/icons";

const { Header } = Layout;

export default function TodoCardList() {
  const { Meta } = Card;

  return (
    <Row align="middle" gutter={[40, 16]}>
      <Col xs={24} sm={12} md={8} xl={6}>
        <Card
          style={{
            maxHeight: 300,
            marginTop: 16,
            minHeight: 130,
            overflowWrap: "break-word",
          }}
        >
          <Meta
            title="Titulo Frase 1"
            description="This is the descriptionnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
          />
        </Card>
      </Col>
    </Row>
  );
}
