import React from "react";
import TournamentForm from "../TournamentForm";
import TournamentsList from "../TournamentsList";

export default function Tournaments() {
  return (
    <div>
      <TournamentsList title="Torneios" />
      <TournamentForm title="Cadastrar Torneio" />
    </div>
  );
}
