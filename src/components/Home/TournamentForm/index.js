import React from "react";

import {
  Form,
  Input,
  Button,
  DatePicker,
  Row,
  Col,
  ConfigProvider,
  Card,
} from "antd";
import "./index.css";
import { InfoCircleOutlined } from "@ant-design/icons";
import moment from "moment";
import "moment/locale/pt-br";
import ptBR from "antd/es/locale/pt_BR";

const { RangePicker } = DatePicker;

export default function TournamentForm(props) {
  const { title } = props;
  const dateFormat = "DD/MM/YYYY";
  return (
    <Card style={{ marginTop: 20 }} title={title}>
      <Form>
        <Row gutter={16} type="flex" gutter={[16]}>
          <Col xs={24} xl={12}>
            <Form.Item
              tooltip={{
                title: "Título do torneio",
                icon: <InfoCircleOutlined />,
              }}
              label="Titulo"
              colon={false}
            >
              <Input placeholder="Título" />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12}>
            <Form.Item
              tooltip={{
                title: "Título do torneio",
                icon: <InfoCircleOutlined />,
              }}
              label="Descrição"
              colon={false}
            >
              <Input placeholder="Descrição" />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12}>
            <Form.Item
              name={["user", "introduction"]}
              tooltip={{
                title: "Informações do torneio de tasks",
                icon: <InfoCircleOutlined />,
              }}
              label="Informações"
              colon={false}
            >
              <Input.TextArea />
            </Form.Item>
          </Col>
          <Col xs={24} xl={12}>
            <Form.Item
              tooltip={{
                title: "Data inicial e final do torneio",
                icon: <InfoCircleOutlined />,
              }}
              label="Periodo"
              colon={false}
            >
              <ConfigProvider locale={ptBR}>
                <RangePicker
                  defaultValue={[
                    moment("2015/01/01", dateFormat),
                    moment("2015/01/01", dateFormat),
                  ]}
                  format={dateFormat}
                />
              </ConfigProvider>
            </Form.Item>
          </Col>

          <Col
            span={24}
            style={{
              textAlign: "right",
            }}
          >
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Cadastrar
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
}
