import React from "react";

import { Card } from "antd";
import { Tabs, Radio } from "antd";

const { TabPane } = Tabs;

export default function TournamentsList(props) {
  const { title } = props;
  return (
    <div>
      <Card style={{ maxHeight: 300 }} title={title}>
        <Tabs defaultActiveKey="1" tabPosition="top" style={{ height: 220 }}>
          {[...Array.from({ length: 10 }, (v, i) => i)].map((i) => (
            <TabPane tab={"Torneio"} key={i}>
              Conteudo do torneio {i}
            </TabPane>
          ))}
        </Tabs>
      </Card>
    </div>
  );
}
