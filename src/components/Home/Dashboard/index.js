import React, { useState } from "react";

import { Menu, Dropdown, Button, Card, Col } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import { CSSTransition } from "react-transition-group";
import "./index.css";

import MotivationalPhrases from "../MotivationalPhrases";
import DoneTodoList from "./../DoneTodoList";
import UndoneTodoList from "./../UndoneTodoList";
import TodoForm from "../TodoForm";

export default function Dashboard(props) {
  const handleMenuClick = (e) => {
    setShowForm(true);
  };

  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">
        <a>Cadastrar frase</a>
      </Menu.Item>
      <Menu.Item key="2">
        <a>Cadastrar tarefa</a>
      </Menu.Item>
    </Menu>
  );

  const handleShowFormEvent = () => {
    setShowForm(false);
  };

  const { todoList, handleAddTodo } = props;
  const [showForm, setShowForm] = useState(false);
  const [showDashboard, setShowDashboard] = useState(true);
  const doneTodoList = todoList.filter((e) => e.done);
  const undoneTodoList = todoList.filter((e) => !e.done);

  return (
    <div>
      <Col
        style={{
          textAlign: "right",
          paddingBottom: "10px",
        }}
      >
        <Dropdown
          disabled={showForm}
          overlay={menu}
          placement="bottomLeft"
          arrow
        >
          <Button type="primary" size="middle">
            <PlusOutlined />
            Novo
          </Button>
        </Dropdown>
      </Col>
      {showDashboard && (
        <div>
          <Card
            style={{
              backgroundImage:
                "url(" +
                "https://free4kwallpapers.com/uploads/wallpaper/forest-mountains-with-a-blue-filter-wallpaper-1024x768-wallpaper.jpg" +
                ")",
            }}
            title="Minhas Frases"
          >
            <MotivationalPhrases />
          </Card>
          <UndoneTodoList undoneTodoList={undoneTodoList} title="Tarefas" />
          <DoneTodoList
            doneTodoList={doneTodoList}
            title="Tarefas concluídas"
          />
        </div>
      )}
      <CSSTransition
        in={showForm}
        timeout={300}
        classNames="alert"
        unmountOnExit
        onEnter={() => setShowDashboard(false)}
        onExited={() => setShowDashboard(true)}
      >
        <TodoForm
          handleAddTodo={handleAddTodo}
          handleShowFormEvent={handleShowFormEvent}
          title="Cadastrar tarefa"
        />
      </CSSTransition>
    </div>
  );
}
