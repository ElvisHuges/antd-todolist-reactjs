import React from "react";

import { Card, Col, Row, Empty } from "antd";

import "./index.css";

export default function UndoneTodoList(props) {
  const { title, undoneTodoList } = props;
  const { Meta } = Card;
  return (
    <Card style={{ marginTop: 16 }} title={title}>
      <Row justify="start" gutter={[40, 16]}>
        {!undoneTodoList.length && <Empty description="Teste" />}

        {undoneTodoList.map((todo, index) => {
          return (
            <Col key={index} xs={24} sm={12} lg={8} md={12} xl={6}>
              <Card
                hoverable
                className="todo-card"
                bodyStyle={{
                  minHeight: 130,
                  overflowWrap: "break-word",
                }}
                style={{ marginTop: 16 }}
                title={todo.title}
              >
                <Meta title="anotação" description={todo.description} />
              </Card>
            </Col>
          );
        })}
      </Row>
    </Card>
  );
}
