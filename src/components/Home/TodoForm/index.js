import React, { useState } from "react";

import { Card, Button, Form, Input, Radio } from "antd";

export default function TodoForm(props) {
  const { title, handleShowFormEvent, handleAddTodo } = props;

  const formRules = [
    {
      required: true,
      message: "Campo obrigatório!",
    },
  ];

  const onFinish = ({ title, description }) => {
    const todo = {
      title: title,
      description: description,
    };
    handleAddTodo(todo);
  };

  return (
    <div>
      <Card
        title={title}
        extra={
          <Button type="primary" onClick={() => handleShowFormEvent()}>
            Voltar
          </Button>
        }
      >
        <Form
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            rules={formRules}
            name="title"
            label="Título"
            required
            tooltip="Título da tarefa"
          >
            <Input />
          </Form.Item>
          <Form.Item rules={formRules} name="description" label="Descrição">
            <Input.TextArea />
          </Form.Item>
          <Form.Item>
            <Button type="primary" size="medium" htmlType="submit">
              Cadastrar
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}
