import React from "react";

import { Card } from "antd";

export default function DoneTodoList(props) {
  const { title, doneTodoList } = props;
  return (
    <div>
      <Card style={{ maxHeight: 300, marginTop: 20 }} title={title}>
        {doneTodoList.map((todo) => {
          return todo.title;
        })}
      </Card>
    </div>
  );
}
