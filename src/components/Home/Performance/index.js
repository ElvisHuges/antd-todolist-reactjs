import "./index.css";
import React from "react";
import MyInfons from "./../MyInfons";
import { Card, Row, Col, Progress } from "antd";
const { Meta } = Card;

export default function Performance(props) {
  return (
    <div>
      <Row gutter={[16, 16]}>
        <Col xs={24} sm={12} md={15} xl={18}>
          <Card title="Meu desenpenho">
            <Row gutter={[40, 16]}>
              <Col xs={12} sm={12} md={12} lg={6}>
                <Progress type="circle" percent={75} />
                <p>Tarefas realizadas</p>
              </Col>
              <Col xs={12} sm={12} md={12} lg={6}>
                <Progress strokeColor="red" type="circle" percent={25} />
                <p>Tarefas pendentes</p>
              </Col>
            </Row>
          </Card>
        </Col>
        <Col xs={24} sm={12} md={9} xl={6}>
          <MyInfons
            username="Elvis Huges"
            description="Engenharia da computação"
          />
        </Col>
      </Row>
    </div>
  );
}
