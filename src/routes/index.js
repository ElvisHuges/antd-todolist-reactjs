import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Login from "./../pages/Login";
import Home from "./../pages/Home";

function Routes() {
  const homeRoutes = [
    {
      path: "/",
      children: () => <Home />,
    },
    {
      path: "/goals",
      children: () => <Home />,
    },
  ];

  return (
    <BrowserRouter>
      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/">
          <Redirect push to="/home/dashboard" />
        </Route>
      </Switch>
      <Route path="/home">
        <Home />
      </Route>
    </BrowserRouter>
  );
}

export default Routes;
