import React from "react";
import Routes from "./routes";
import "./styles/css/antd.css";
import { UserProvider } from "./store/user";

export default class App extends React.Component {
  render() {
    return (
      <UserProvider>
        <Routes />
      </UserProvider>
    );
  }
}
