import React, { createContext, useReducer } from "react";

export const AuthContext = createContext();

const initialState = {
  isAuthenticated: false,
  todoList: [
    { title: "Todo Teste", description: "small testes" },
    {
      title: "Todo Teste",
      description: "Bigggggggggggggggggggggggggggggggggggggggggggg Description",
    },
  ],
  username: null,
  token: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("username", JSON.stringify(action.payload.username));
      localStorage.setItem("token", JSON.stringify(action.payload.token));

      return {
        ...state,
        isAuthenticated: true,
        username: action.payload.username,
        token: action.payload.token,
      };
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    case "ADDTODO":
      return {
        ...state,
        todoList: [...state.todoList, action.payload],
      };
    default:
      return state;
  }
};
export function UserProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };
  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
}
