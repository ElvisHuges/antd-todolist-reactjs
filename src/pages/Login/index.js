import React, { useContext } from "react";
import { useHistory } from "react-router-dom";

import { Form, Input, Button, Card, Row, Col, Checkbox } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

import "./index.css";
import { AuthContext } from "../../store/user";

/* eslint-disable no-template-curly-in-string */

/* eslint-enable no-template-curly-in-string */

export default function Login(props) {
  const history = useHistory();
  const { state, dispatch } = useContext(AuthContext);

  const onFinish = (values) => {
    const payload = {
      username: values.username,
      token: "SDASD-ASQWEQW-dASDASD",
    };
    dispatch({ type: "LOGIN", payload: payload });
    history.push("/home/dashboard");
  };

  return (
    <Card title="Login" bordered={false}>
      <Row justify="center" align="middle" style={{ minHeight: "70vh" }}>
        <Col md={{ span: 8, offset: 0 }} xs={{ span: 22, offset: 0 }}>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            size="large"
            onFinish={onFinish}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: "Campo obrigatório!",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Campo obrigatório!",
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Lembrar-me</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="">
                Esqueci minha senha
              </a>
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Entrar
              </Button>
              ou <a href="">Cadastrar-se!</a>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Card>
  );
}
