import "./index.css";
import React, { useState, useContext } from "react";
import { Route, Switch, Link, useLocation, useHistory } from "react-router-dom";

import { Layout, Menu } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { AuthContext } from "../../store/user";

import Dashboard from "./../../components/Home/Dashboard";
import Tournaments from "../../components/Home/Tournaments";
import Performance from "../../components/Home/Performance";

const { Header, Content, Footer, Sider } = Layout;

export default function Home(props) {
  const [collapsed, setCollapsed] = useState(true);

  const { state, dispatch } = useContext(AuthContext);
  const contextTodoList = state.todoList;

  const location = useLocation();
  const history = useHistory(0);

  const handleMenuClick = ({ key }) => {
    if (key === "logout") {
      dispatch({ type: "LOGOUT" });
      history.push("/login");
    }
    setCollapsed(!collapsed);
  };

  const handleAddTodo = (todo) => {
    console.log("handleAddTodo", todo);
    dispatch({ type: "ADDTODO", payload: todo });
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        style={{
          overflow: "auto",
          left: 0,
        }}
        breakpoint="lg"
        collapsedWidth="0"
        trigger={null}
        collapsible
        collapsed={collapsed}
      >
        <div className="logo" />
        <Menu
          theme="dark"
          onClick={(key) => handleMenuClick(key)}
          mode="inline"
          defaultSelectedKeys={[location.pathname]}
        >
          <Menu.Item key="/home/dashboard">
            <Link to="/home/dashboard">Dashboard</Link>
          </Menu.Item>
          <Menu.Item key="/home/performance">
            <Link to="/home/performance">Desempenho</Link>
          </Menu.Item>
          <Menu.Item key="/home/tournaments">
            <Link to="/home/tournaments">Torneios</Link>
          </Menu.Item>

          <Menu.Item key="4">Configurações</Menu.Item>
          <Menu.Item key="logout">Logout</Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </Header>
        <Content
          style={{
            margin: "20px 16px",
          }}
        >
          <Switch>
            <Route path="/home/dashboard/">
              <Dashboard
                handleAddTodo={handleAddTodo}
                todoList={contextTodoList}
              />
            </Route>
            <Route path="/home/tournaments/">
              <Tournaments />
            </Route>
            <Route exact path="/home/performance/">
              <Performance />
            </Route>
          </Switch>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Elvis Huges
        </Footer>
      </Layout>
    </Layout>
  );
}
